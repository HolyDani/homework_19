#include <iostream>

class Animals
{
public:
    virtual void Voice() = 0;
    
};

class Dog : public Animals
{
public:
    void Voice() override
    {
        std::cout << "DOG SAY: Woof-Woof!\n\n";
    }
};

class Cat : public Animals
{
public:
    void Voice() override
    {
        std::cout << "CAT SAY: Meow-Meow!\n\n";
    }
};


class Fish : public Animals
{
public:
    void Voice() override
    {
        std::cout << "FISH SAY: ...........! <0==< \n";
    }
};

int main()
{
    const int LENGTH = 3;
    Animals* arr[LENGTH];
    arr[0] = new Dog();
    arr[1] = new Cat();
    arr[2] = new Fish();

    for (Animals* elem : arr)
    {
        elem->Voice();
    }
    return 0;
}